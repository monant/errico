<?php

class SiteController extends Controller {

    public $baseTitle = "Ing. Angelo Errico";

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        $this->pageTitle = $this->baseTitle . ' - Homepage';
        $this->render('index');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $this->pageTitle = $this->baseTitle . ' - Contatti';
        $this->layout = "main_page";
        $this->render('contact', array());
    }

    public function actionCostruzioni_edili() {
        $this->pageTitle = $this->baseTitle . ' - Costruzioni Edili';
        $this->layout = "main_page";
        $this->render('costruzioniedili', array());
    }

    public function actionRistrutturazioni() {
        $this->pageTitle = $this->baseTitle . ' - Ristrutturazioni';
        $this->layout = "main_page";
        $this->render('ristrutturazioni', array());
    }

    public function actionConsulenze_tecniche() {
        $this->pageTitle = $this->baseTitle . ' - Consulenze tecniche';
        $this->layout = "main_page";
        $this->render('consulenzetecniche', array());
    }

    public function actionStudio_tecnico() {
        $this->pageTitle = $this->baseTitle . ' - Studio tecnico';
        $this->layout = "main_page";
        $this->render('studiotecnico', array());
    }

    public function actionCalcoli_strutturali() {
        $this->pageTitle = $this->baseTitle . ' - Calcoli strutturali';
        $this->layout = "main_page";
        $this->render('calcolistrutturali', array());
    }

    public function actionCatasto() {
        $this->pageTitle = $this->baseTitle . ' - Catasto';
        $this->layout = "main_page";
        $this->render('catasto', array());
    }

    public function actionImmobili() {
        $this->pageTitle = $this->baseTitle . ' - Immobili';
        $this->layout = "main_page";
        $criteria = new CDbCriteria();
        $criteria->order = "posizione ASC";
        $criteria->condition = "eliminato = 0";
        $localitas = Localita::model()->findAll($criteria);

        $this->render('immobili', array(
            'localitas' => $localitas
        ));
    }

    public function actionMail() {
        
    }

    public function actionImmobile($id) {
                $this->pageTitle = $this->baseTitle . ' - Immobili';
        $this->layout = "main_page";
        $model = Immobile::model()->findByPk($id);
        if (!isset($model) || $model->pubblicato == 0 || $model->eliminato == 1) {
            $this->redirect($this->createUrl('/immobili'));
        }
        $this->render('immobile', array('model' => $model));
    }

}
