<?php

/**
 * This is the model class for table "localita".
 *
 * The followings are the available columns in table 'localita':
 * @property integer $id
 * @property string $nome
 * @property integer $posizione
 * @property integer $eliminato
 *
 * The followings are the available model relations:
 * @property Immobile[] $immobiles
 */
class Localita extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'localita';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nome, eliminato', 'required'),
            array('posizione, eliminato', 'numerical', 'integerOnly' => true),
            array('nome', 'length', 'max' => 256),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nome, posizione, eliminato', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'immobiles' => array(self::HAS_MANY, 'Immobile', 'id_localita'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nome' => 'Nome',
            'posizione' => 'Posizione',
            'eliminato' => 'Eliminato',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->order = 'posizione ASC';

        $criteria->compare('id', $this->id);
        $criteria->compare('nome', $this->nome, true);
        $criteria->compare('posizione', $this->posizione);
        $criteria->compare('eliminato', 0);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Localita the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getImmobili() {
        $criteria = new CDbCriteria();
        $criteria->condition = "pubblicato = 1 AND eliminato = 0 AND id_localita = ".$this->id;
        $criteria->order = "posizione ASC";
        return Immobile::model()->findAll($criteria);
    }

}
