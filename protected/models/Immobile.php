<?php

/**
 * This is the model class for table "immobile".
 *
 * The followings are the available columns in table 'immobile':
 * @property integer $id
 * @property integer $id_localita
 * @property string $tipologia
 * @property string $indirizzo
 * @property string $prezzo
 * @property string $informazioni
 * @property string $metratura
 * @property string $piano
 * @property string $locali
 * @property string $bagno
 * @property string $esterno
 * @property string $ascensore
 * @property string $riscaldamento
 * @property string $giardino
 * @property string $box
 * @property string $classe_energetica
 * @property string $realizzazione
 * @property string $latitudine
 * @property string $longitudine
 * @property integer $pubblicato
 * @property integer $posizione
 * @property integer $eliminato
 *
 * The followings are the available model relations:
 * @property Gallery[] $galleries
 * @property Localita $idLocalita
 */
class Immobile extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'immobile';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id_localita, tipologia, indirizzo, latitudine, longitudine, pubblicato, posizione, eliminato', 'required'),
            array('id_localita, pubblicato, posizione, eliminato', 'numerical', 'integerOnly' => true),
            array('tipologia, indirizzo, prezzo, metratura, piano, locali, bagno, esterno, ascensore, riscaldamento, giardino, box, classe_energetica, realizzazione, latitudine, longitudine', 'length', 'max' => 256),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, id_localita, tipologia, indirizzo, prezzo, informazioni, metratura, piano, locali, bagno, esterno, ascensore, riscaldamento, giardino, box, classe_energetica, realizzazione, latitudine, longitudine, pubblicato, posizione, eliminato', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'galleries' => array(self::HAS_MANY, 'Gallery', 'id_immobile'),
            'idLocalita' => array(self::BELONGS_TO, 'Localita', 'id_localita'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'id_localita' => 'Localita',
            'tipologia' => 'Tipologia',
            'indirizzo' => 'Indirizzo',
            'prezzo' => 'Prezzo',
            'informazioni' => 'Informazioni',
            'metratura' => 'Metratura',
            'piano' => 'Piano',
            'locali' => 'Locali',
            'bagno' => 'Bagno',
            'esterno' => 'Esterno',
            'ascensore' => 'Ascensore',
            'riscaldamento' => 'Riscaldamento',
            'giardino' => 'Giardino',
            'box' => 'Box',
            'classe_energetica' => 'Classe Energetica',
            'realizzazione' => 'Realizzazione',
            'latitudine' => 'Latitudine',
            'longitudine' => 'Longitudine',
            'pubblicato' => 'Pubblicato',
            'posizione' => 'Posizione',
            'eliminato' => 'Eliminato',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->order = 'posizione ASC';

        $criteria->compare('id', $this->id);
        $criteria->compare('id_localita', $this->id_localita);
        $criteria->compare('tipologia', $this->tipologia, true);
        $criteria->compare('indirizzo', $this->indirizzo, true);
        $criteria->compare('prezzo', $this->prezzo, true);
        $criteria->compare('informazioni', $this->informazioni, true);
        $criteria->compare('metratura', $this->metratura, true);
        $criteria->compare('piano', $this->piano, true);
        $criteria->compare('locali', $this->locali, true);
        $criteria->compare('bagno', $this->bagno, true);
        $criteria->compare('esterno', $this->esterno, true);
        $criteria->compare('ascensore', $this->ascensore, true);
        $criteria->compare('riscaldamento', $this->riscaldamento, true);
        $criteria->compare('giardino', $this->giardino, true);
        $criteria->compare('box', $this->box, true);
        $criteria->compare('classe_energetica', $this->classe_energetica, true);
        $criteria->compare('realizzazione', $this->realizzazione, true);
        $criteria->compare('latitudine', $this->latitudine, true);
        $criteria->compare('longitudine', $this->longitudine, true);
        $criteria->compare('pubblicato', $this->pubblicato);
        $criteria->compare('posizione', $this->posizione);
        $criteria->compare('eliminato', 0);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Immobile the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getLocalitas() {
        $toret = array();
        $criteria = new CDbCriteria();
        $criteria->order = "nome ASC";
        foreach (Localita::model()->findAll($criteria) as $localita):
            $toret[$localita->id] = $localita->nome;
        endforeach;
        return $toret;
    }

    public function getLocalita() {
        $m = Localita::model()->findByPk($this->id_localita);
        if (isset($m))
            return $m->nome;
        return 'n.d.';
    }

    public function getCover() {
        $criteria = new CDbCriteria();
        $criteria->condition = "id_immobile = $this->id";
        $criteria->limit = 1;
        $criteria->order = "posizione ASC";
        $model = Gallery::model()->find($criteria);
        if (!isset($model))
            return Yii::app()->getBaseUrl(true) . '/images/cf_1.jpg';
        else
            return Yii::app()->getBaseUrl(true) . '/uploads/gallery/' . $model->filename;
    }

    public function getGallery() {
        $criteria = new CDbCriteria();
        $criteria->condition = "id_immobile = $this->id";
        $criteria->order = "posizione ASC";
        return $models = Gallery::model()->findAll($criteria);
    }

}
