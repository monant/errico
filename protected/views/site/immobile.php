<section class="section" id="basic">
    <div class="container">
        <div class="row">
            <div class="jt_col col-md-8 jt_col col-md-offset-2 text-center">
                <h2 class="title main"><?php echo $model->idLocalita->nome ?></h2>
            </div>
        </div>
        <div class="project-related">
            <div class="row">
                <div class="jt_col col-md-10 col-md-offset-1 text-center">
                    <h2 class="title fz35"><?php echo $model->indirizzo ?></h2>
                    <h2 class="title fz25" style="color:green;"><?php echo $model->tipologia ?></h2>
                </div>
            </div>                
        </div>
    </div>
    <div class="project-carousel">
        <?php
        $models = $model->getGallery();
        if (count($models)) {
            ?>
            <div id="owl-demo" class="owl-carousel owl-theme">
                <?php foreach ($models as $model_gallery): ?>
                    <div class="item"><img src="<?php echo Yii::app()->getBaseUrl(true) . '/uploads/gallery/' . $model_gallery->filename ?>"></div>
                        <!--<img src="<?php echo Yii::app()->getBaseUrl(true) . '/uploads/gallery/' . $model_gallery->filename ?>" class="img-responsive" alt="Lorem">-->
                <?php endforeach; ?>
            </div>
        <?php } ?>
    </div>
    <?php if ($model->prezzo != '') { ?>
        <div class="container">
            <div class="row">
                <div class="jt_col col-md-10 col-md-offset-1 text-center" style="margin-bottom: 100px;">
                    <div id="costo" style="margin:auto; min-width:210px; height:35px; background-color:darkgreen; border-radius: 20px; width: max-content; padding: 0px 20px;">
                        <div id="img" style="margin:auto; width:60px; height:35px; float:left;"><img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/icovendita.png" style="width: 35px; height: 35px;"></div>
                        <div id="prezzo" style="margin:auto; min-width:150px; height:35px; color:white; float:left; font-size:25px;">
                            <?php echo $model->prezzo ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<div class="container bigger">
    <div class="row">
        <div class="col-md-5 project-data">
            <ul>
                <?php if ($model->metratura != '') { ?>
                    <li class="clearfix"><h3 class="title"><strong>METRATURA</strong></h3> <h3 class="data"><?php echo $model->metratura ?></h3></li>
                <?php } ?>
                <?php if ($model->tipologia != '') { ?>
                    <li class="clearfix"><h3 class="title"><strong>TIPOLOGIA</strong></h3> <h3 class="data"><?php echo $model->tipologia ?></h3></li>
                <?php } ?>
                <?php if ($model->piano != '') { ?>
                    <li class="clearfix"><h3 class="title"><strong>PIANO</strong></h3> <h3 class="data"><?php echo $model->piano ?></h3></li>
                <?php } ?>
                <?php if ($model->locali != '') { ?>
                    <li class="clearfix"><h3 class="title"><strong>LOCALI</strong></h3> <h3 class="data"><?php echo $model->locali ?></h3></li>
                <?php } ?>
                <?php if ($model->bagno != '') { ?>
                    <li class="clearfix"><h3 class="title"><strong>BAGNO</strong></h3> <h3 class="data"><?php echo $model->bagno ?></h3></li>
                <?php } ?>
                <?php if ($model->esterno != '') { ?>
                    <li class="clearfix"><h3 class="title"><strong>ESTERNO</strong></h3> <h3 class="data"><?php echo $model->esterno ?></h3></li>
                <?php } ?>
                <?php if ($model->ascensore != '') { ?>
                    <li class="clearfix"><h3 class="title"><strong>ASCENSORE</strong></h3> <h3 class="data"><?php echo $model->ascensore ?></h3></li>
                <?php } ?>
                <?php if ($model->riscaldamento != '') { ?>
                    <li class="clearfix"><h3 class="title"><strong>RISCALDAMENTO</strong></h3> <h3 class="data"><?php echo $model->riscaldamento ?></h3></li>
                <?php } ?>
                <?php if ($model->giardino != '') { ?>
                    <li class="clearfix"><h3 class="title"><strong>GIARDINO</strong></h3> <h3 class="data"><?php echo $model->giardino ?></h3></li>
                <?php } ?>
                <?php if ($model->box != '') { ?>
                    <li class="clearfix"><h3 class="title"><strong>BOX</strong></h3> <h3 class="data"><?php echo $model->box ?></h3></li>
                <?php } ?>
                <?php if ($model->classe_energetica != '') { ?>
                    <li class="clearfix"><h3 class="title"><strong>CLASSE ENERGETICA</strong></h3> <h3 class="data"><?php echo $model->classe_energetica ?></h3></li>
                <?php } ?>
                <?php if ($model->realizzazione != '') { ?>
                    <li class="clearfix"><h3 class="title"><strong>REALIZZAZIONE</strong></h3> <h3 class="data"><?php echo $model->realizzazione ?></h3></li>
                <?php } ?>
            </ul>
            <!--<a href="#" class="button fill">Go Live</a>-->
        </div>
        <div class="col-md-5 col-md-offset-2 project-text">
            <?php if ($model->informazioni != '') { ?>
                <h3 class="clearfix" class="title"><strong>INFORMAZIONI</strong></h3>
                <?php echo nl2br($model->informazioni) ?>
            <?php } ?>
        </div>
    </div>

</div>
<div class="container">
    <div class="row">
        <div class="project-related">
            <div class="row">
                <div class="jt_col col-md-10 col-md-offset-1 text-center">
                    <h2 class="title fz30">LOCALIZZAZIONE APPARTAMENTO</h2>
                </div>
            </div>
            <div class="row">
                <div class='map' id='map_canvas' style="width: 800px; height: 300px; margin: auto">
                </div>
<!--                <iframe src="https://www.google.com/maps/embed?pb=!1m13!1m11!1m3!1d1203.8487390029077!2d<?= $model->longitudine ?>!3d<?= $model->latitudine ?>!2m2!1f0!2f0!3m2!1i1024!2i768!4f13.1!5e1!3m2!1sit!2sit!4v1515456642250" width="800" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>-->
            </div>

        </div>
    </div>
</div>
</section>
<!-- END BASIC SECTION -->
<script type="text/javascript">
    function initMapImmobile() {
        var immobile = {lat: <?= $model->latitudine ?>, lng: <?= $model->longitudine ?>};
        var map = new google.maps.Map(document.getElementById('map_canvas'), {
            zoom: 16,
            center: immobile
        });
        var marker = new google.maps.Marker({
            position: immobile,
            map: map
        });
    }

    initMapImmobile();
</script>
<style type="text/css">
    #owl-demo .item img {
        display: block;
        width: 100%;
        height: auto; }

    .owl-theme .owl-controls {
        position: relative; }
    .owl-theme .owl-controls .owl-page {
        position: relative;
        display: inline-block;
        width: 6em;
        height: 3em;
        margin: 0 0.25em;
        outline: none;
        transition: all 0.1s ease-in-out; }
    .owl-theme .owl-controls .owl-page:focus {
        outline: none; }
    .owl-theme .owl-controls .owl-page span {
        display: none; }
    .owl-theme .owl-controls .owl-page.active {
        transform: scale(1.1); }
    .owl-theme .owl-controls .owl-page:not(.active) {
        transform: scale(0.8);
        -webkit-box-shadow: inset 0 0 10em 0 rgba(0, 0, 0, 0.75);
        box-shadow: inset 0 0 10em 0 rgba(0, 0, 0, 0.75); }
    .owl-theme .owl-controls .owl-page:not(.active):hover {
        transform: scale(0.9); }
    .owl-theme .owl-controls .owl-buttons .owl-prev,
    .owl-theme .owl-controls .owl-buttons .owl-next {
        font-size: 2em;
        display: block;
        position: absolute;
        top: 0;
        line-height: 0.75em;
        width: 1em;
        height: 1em;
        border-radius: 1px;
        background-color: #c0c0c0; }
    .owl-theme .owl-controls .owl-buttons .owl-prev:focus,
    .owl-theme .owl-controls .owl-buttons .owl-next:focus {
        -webkit-box-shadow: inset 0 0 10em 0 rgba(255, 0, 0, 0.75);
        box-shadow: inset 0 0 10em 0 rgba(255, 0, 0, 0.75); }
    .owl-theme .owl-controls .owl-buttons .owl-prev {
        left: 0.25em; }
    .owl-theme .owl-controls .owl-buttons .owl-next {
        right: 0.25em; }

    .owl-prev, .owl-next{
        display: none !important;
    }

</style>