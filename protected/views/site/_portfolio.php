<!-- PORTFOLIO -->
<section class="section dark" id="projects">
    <div class="container">
        <div class="row">
            <div class="jt_col col-md-8 jt_col col-md-offset-2 text-center">
                <h2 class="title main">I Nostri Progetti</h2>
            </div>
        </div>
    </div>
</section>
<section class="section dark full-width" id="works">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <ul class="filter">
                    <li class="filter-item"><a data-filter="*" class="active filter-link" href="#">Tutti</a></li>
                    <li class="filter-item"><a data-filter=".architecture" class="filter-link" href="#">Architettura</a></li>
                    <li class="filter-item"><a data-filter=".interior" class="filter-link" href="#">Appartamenti</a></li>
                    <li class="filter-item"><a data-filter=".construction" class="filter-link" href="#">Esterni</a></li>
                </ul>
                <div class="folio-grid clearfix voffset100">
                    <?php
                    $categories = array('architecture' => 'architecture', 'interior' => 'interior', 'construction' => 'construction');
                    for ($i = 1; $i < 10; $i++):
                        ?>
                        <article class="folio-item <?php echo array_rand($categories) ?> col-sm-4">
                            <a href="<?php echo $this->createUrl("/site/work", array('id' => $i)); ?>" class="folio-link">
                                <img src="http://placehold.it/853x568" class="img-responsive" alt=""/>
                                <div class="folio-over">
                                    <div class="folio-inner">
                                        <div class="folio-data">
                                            <h4 class="folio-name">uniform Vinyl House</h4>
                                            <h5 class="folio-type">Personal, Photography</h5>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </article>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END PORTFOLIO -->