<section class="section" id="basic">
    <div class="container" style="margin-bottom:30px;">
        <div class="row">
            <div class="jt_col col-md-8 jt_col col-md-offset-2 text-center">
                <h2 class="title main">IMMOBILI</h2>
            </div>
        </div>
        <div class="row">
            <div class="jt_col col-md-10 col-md-offset-1 text-center">
                <?php foreach ($localitas as $key => $localita): 
                    if($key != 0){
                        echo "&shy; - &shy;";
                    }
                    ?>
                    <a href="#sezione<?php echo $localita->id ?>" class="wwd-menuimm"><?php echo $localita->nome ?></a> 
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
<!-- IMMAGINE ESEMPIO-->
<div class="container">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1 post-image">
            <div class="generic-carousel" data-animation-out="slideOutRight" data-animation-in="slideInLeft" data-dots="false">
                <div class="item">
                    <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/immobili.png" alt="" class="img-responsive">
                </div>
                <div class="item">
                    <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/immobili.png" alt="" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- FINE IMMAGINE ESEMPIO-->

<?php foreach ($localitas as $localita): ?>
    <!--SEZIONE LOCALITA -->
    <div class="container">
        <section id="sezione<?php echo $localita->id ?>"></section>
        <div class="row">
            <div class="jt_col col-md-8 jt_col col-md-offset-2 text-center">
                <h2 class="title main"><?php echo $localita->nome ?></h2>
            </div>
        </div>
        <div class="row voffset100">
            <?php
            $immobili = $localita->getImmobili();
            $class = "col-sm-4";
            if (count($immobili) == 1) {
                $class = "col-sm-4 col-sm-offset-4";
            } else if (count($immobili) == 2) {
                $class = "col-sm-4 col-sm-offset-1";
            }
            foreach ($immobili as $model):
                ?>
                <div class="<?php echo $class ?>">
                    <div class="wwd-block">
                        <a href="<?php echo $this->createUrl('/site/immobile', array('id' => $model->id)) ?>">
                            <div class="immobile-item-list" style="background-image: url('<?php echo $model->getCover() ?>')"></div>                            
                        </a>
                        <div class="wwd-data">
                            <h3 class="wwd-title"><?php echo $model->indirizzo ?></h3>
                            <h2 class="wwd-appart"><?php echo $model->tipologia ?></h2>
                            <a href="<?php echo $this->createUrl('/site/immobile', array('id' => $model->id)) ?>" class="wwd-link">DETTAGLI</a>
                        </div>
                    </div>
                </div>
    <?php endforeach; ?>
        </div>
    </div>
    <!--FINE SEZIONE LOCALITA-->
<?php endforeach; ?>