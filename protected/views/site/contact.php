<!-- CONTACT -->
<section class="section" id="contact">
    <div class="container">
        <div class="row">
            <div class="jt_col-col-md-12 text-center voffset100">
                <h3 class="title main voffset100">CONTATTI</h3>
                <h3 class="title fz20 voffset0">VUOI UNA CONSULENZA?</h3>
                <h3 class="title primary fz50">+39 0974 824945</h3>
            </div>
            <div class="jt_col-col-md-12 voffset100">
                <form id="contactform" action="<?php echo $this->createUrl('/site/mail') ?>" method="post">
                    <div class="row text-center contact-form">
                        <div class="jt_col col-md-4">
                            <input id="name" name="name" type="text" class="required form-control" placeholder="Nome">
                        </div>
                        <div class="jt_col col-md-4">
                            <input id="email" name="email" type="text" class="required form-control" placeholder="Email">
                        </div>
                        <div class="jt_col col-md-4">
                            <input id="phone" name="phone" type="text" class="form-control" placeholder="Telefono">
                        </div>
                        <div class="jt_col col-md-12">
                            <textarea id="message" name="message" class="required form-control" placeholder="Messaggio"></textarea>
                        </div>
                        <div class="jt_col col-md-4 col-md-offset-4"><button type="submit" class="button fill">INVIA</button></div>
                        <div class="jt_col col-md-4 col-md-offset-4">
                            <div class="formSent success"><strong>Messaggio inviato con successo.</strong> Grazie per averci contattato!</div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="jt_col-col-md-12 voffset100 text-center">
                <h4 class="title fz16">ING. ANGELO ERRICO</h4>
                <h4 class="title fz16">via Difesa 6, Agropoli (SA)</h4>
                <h4 class="title fz16">TEL\FAX 0974 824945 | P.IVA 48789562</h4>
                <h4 class="title fz16 primary voffset60">APPALTITEMAIMPIANTI@LIBERO.IT</h4>
            </div>
        </div>
    </div>
</section>
<!-- END CONTACT -->