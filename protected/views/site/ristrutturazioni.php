<div class="page-header single concrete">
    <div class="title light">
        <h1 class="light fz50">RISTRUTTURAZIONI</h1>
    </div>
</div>

<section class="blog single">
    <span class="prev-post"><a href="<?php echo $this->createUrl('/costruzioni-edili'); ?>"> Indietro</a></span>
    <span class="next-post"><a href="<?php echo $this->createUrl('/consulenze-tecniche'); ?>">Avanti > </a></span>
    <div class="container">
        <article class="post">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 post-image">
                    <div class="generic-carousel" data-animation-out="slideOutRight" data-animation-in="slideInLeft" data-dots="false">
                        <div class="item">
                            <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/minislide/ristrutturazioni_1.jpg" alt="Ristrutturazioni Edili" class="img-responsive">
                        </div>
                        <div class="item">
                            <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/minislide/ristrutturazioni_1.jpg" alt="Ristrutturazioni Edili" class="img-responsive">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <h2 class="title fz26 upper">RISTRUTTURAZIONI</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 post-content">
                    <div class="text">
                        <p>Le <strong>ristrutturazioni</strong> e il <strong>restauro conservativo</strong> sono uno dei punti di forza dell’impresa TEMA IMPIANTI S.R.L.</p><br>
                        <p>L’impresa è specializzata nella ristrutturazione di appartamenti, edifici, uffici, attività commerciali e ville. Ci occupiamo anche del recupero dal punto di vista sismico delle vecchie murature, tramite l’utilizzo di fibre di carbonio, fibre di vetro, iniezioni con resine epossidiche, cuci e scuci delle murature, integrazione con elementi in profilato metallico, cerchiature di aperture e tutto ciò che riguarda la struttura esistente.</p><br>
                        <p>Riportare facciate e interni al loro antico splendore è un’arte che richiede esperienza e competenza nel settore, oltre a buon gusto e cultura; è per questo che ci impegniamo ad aggiungere soluzioni pratiche e moderne senza danneggiare lo stile del rustico e l’eleganza dell’antico.</p><br>
                        <p>Importanti sono le scelte dei materiali, delle lavorazioni e del recupero degli elementi riutilizzabili, scelti appositamente per rispettare l’atmosfera della casa e il piacere del cliente; per riuscire nelle ristrutturazioni è importante anche avere una conoscenza storica e culturale di ciò che si sta trattando, conoscere per rinnovare e non per sconvolgere.</p><br>
                        <p>Di seguito un elenco non esaustivo dei possibili servizi offerti in fase di ristrutturazione dall’impresa:</p>
                        -	Recupero architettonico e strutturale murature danneggiate<br>
                        -	Restauro completo di appartamenti<br>
                        -	Rifacimenti e restauri facciate<br>
                        -	Risanamenti e deumidificazioni<br>
                        -	Risanamenti intonaci e tinteggiatura<br>
                        -	Rifacimenti coperture e lastrici solari<br>
                        -	Isolamenti e coibentazioni termiche tetti, solai e pareti<br>
                        -	Rifacimento impianti - termico/idrico-sanitario/elettrico e certificazioni come per legge<br><br>
                        <p>Vuoi ristrutturare la tua casa? Richiedi un preventivo gratuito. Lo studio tecnico si occuperà delle redazione del computo metrico e della stima dei costi da sostenere.</p>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>