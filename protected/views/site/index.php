<section id="home-slider">
    <div id="owl-main" class="owl-carousel">
        <div class="item">
            <div class="overlay"></div>
        </div>
        <div class="item">
            <div class="overlay"></div>
        </div>
        <div class="item">
            <div class="overlay"></div>
        </div>
    </div>
    <div class="slide-content">
        <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/slide-logo.png" alt="" class="slide-logo">
        <h3 class="slide-title">La nostra esperienza, al tuo servizio.</h3>
        <div id="owl-text" class="container" style="padding-bottom: 30px">
            <div class="col-sm-12">
                <h2>PROGETTIAMO E CONCRETIZZIAMO LA TUA CASA</h2>
            </div>
        </div>
        <a href="<?php echo $this->createUrl('/immobili') ?>" class="button fill">IMMOBILI IN VENDITA</a>
    </div>
    <div class="scroll"><span></span></div>
</section>
<!-- END SLIDER -->

<!-- PRESENTATION -->
<section id="presentation" class="section full-width dark">
    <div class="container">
        <div class="row">
            <!--            <div class="jt_col col-md-6">
                            <div class="carousel-wrap">
                                <div id="owl-studio" class="generic-carousel" data-animation-out="slideOutRight" data-animation-in="slideInLeft" data-dots="false">
                                    <div class="item">
                                        <img class="img-responsive" src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/home.jpg" alt="">
                                        <div class="slide-content"> 
                                            <div class="vertical-align">
                                                <h2 class="title light fz40">Soluzioni personalizzate</h2>
                                                <h3 class="subtitle light fz18">Personalizziamo tutte le tue idee per la costruzione o la ristrutturazione del tuo immobile.</h3>
                                                <a href="<?php echo $this->createUrl('/contatti') ?>" class="button">CONTATTACI</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <img class="img-responsive" src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/soluzioni.jpg" alt="">
                                        <div class="slide-content"> 
                                            <div class="vertical-align">
                                                <h2 class="title light fz40">Soluzioni pronte</h2>
                                                <h3 class="subtitle light fz18">Abbiamo immobili nuovi già pronti e disponibili per la vendita, scopri le soluzioni migliori per te!</h3>
                                                <a href="<?php echo $this->createUrl('/immobili') ?>" class="button">SCOPRI</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="jt_col col-md-6 text-center align-table">
                            <div class="row align-table-cell">
                                <div class="jt_col col-md-8 jt_col col-md-offset-2">
                                    <div class="icon light fz30"><i class="icon-home"></i></div>
                                    <h2 class="title light fz40">partiamo da zero</h2>
                                    <h3 class="subtitle light fz18">Costruiamo immobili dalle fondamenta, seguendo rigorosamente una deontologia dedita alla creazione della tua casa dalle fondamenta al primo mattone.</h3>
                                </div>
                            </div>
                        </div>-->
        </div>
    </div>
</section>
<!-- END PRESENTATION -->

<?php $this->renderPartial('/site/_services') ?>

<!-- CERTIFICATIONS -->
<section id="certifications" class="section">
    <div class="container">
        <div class="row">
            <div class="jt_col col-md-8 jt_col col-md-offset-2 text-center">
                <h2 class="title main">CERTIFICAZIONI</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="service text-center">
<!--                    <div class="icon fz32"><i class="icon fa fa-clock-o"></i></div>-->
                    <h2 class="title fz20">OG1</h2>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="service text-center">
<!--                    <div class="icon fz32"><i class="icon fa fa-line-chart"></i></div>-->
                    <h2 class="title fz20">OG3</h2>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="service text-center">
<!--                    <div class="icon fz32"><i class="icon fa fa-desktop"></i></div>-->
                    <h2 class="title fz20">OG6</h2>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="service text-center">
<!--                    <div class="icon fz32"><i class="icon fa fa-comments-o"></i></div>-->
                    <h2 class="title fz20">OG10</h2>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="service text-center">
<!--                    <div class="icon fz32"><i class="icon fa fa-line-chart"></i></div>-->
                    <h2 class="title fz20">OG11</h2>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="service text-center">
<!--                    <div class="icon fz32"><i class="icon fa fa-desktop"></i></div>-->
                    <h2 class="title fz20">OG24</h2>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="service text-center">
<!--                    <div class="icon fz32"><i class="icon fa fa-comments-o"></i></div>-->
                    <h2 class="title fz20">OS30</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-center voffset170">

            </div>
        </div>
    </div>
</section>
<!-- END CERTIFICATIONS -->


<?php //$this->renderPartial('/site/_portfolio') ?>

<?php $this->renderPartial('/site/_team') ?>

<?php
$m = ValoriDefault::model()->findByPk(1);
?>

<!-- COUNTERS -->
<section class="section nopadding" id="counters">
    <div class="container">
        <div class="row">
            <div class="jt_col col-sm-6 col-md-3">
                <div class="counter clearfix" data-count="<?php echo $m->field1 ?>">
                    <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/1.png" alt="fa-home"/>
                    <div class="data">
                        <span class="number">0</span>
                        <span class="literal">Appartamenti <br/>in vendita</span>
                    </div>
                </div>
            </div>
            <div class="jt_col col-sm-6 col-md-3">
                <div class="counter clearfix" data-count="<?php echo $m->field2 ?>">
                    <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/2.png" alt="fa-home"/>
                    <div class="data">
                        <span class="number">0</span>
                        <span class="literal">Appartamenti<br/> venduti</span>
                    </div>
                </div>
            </div>
            <div class="jt_col col-sm-6 col-md-3">
                <div class="counter clearfix" data-count="<?php echo $m->field3 ?>">
                    <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/3.png" alt="fa-home"/>
                    <div class="data">
                        <span class="number">0</span>
                        <span class="literal">Cantieri aperti</span>
                    </div>
                </div>
            </div>
            <div class="jt_col col-sm-6 col-md-3">
                <div class="counter clearfix" data-count="<?php echo $m->field4 ?>">
                    <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/4.png" alt="fa-home"/>
                    <div class="data">
                        <span class="number">0</span>
                        <span class="literal">Cantieri ultimati</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END COUNTERS -->

<!-- OUR COMPANY -->
<section id="company" class="section">
    <div class="container">
        <div class="row voffset100">
            <div class="col-sm-10 col-sm-offset-1">
                <h2 class="title main">AZIENDA</h2>
                <div class="generic-carousel" data-animation-out="slideOutRight" data-animation-in="slideInLeft" data-dots="false">
                    <div class="item">
                        <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/azienda1.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="item">
                        <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/azienda2.jpg" alt="" class="img-responsive">
                    </div>
                </div>
                <h2 class="post title">DAL 1994 AL TUO SERVIZIO</h2>
                <div class="text-content">
                    <p>L'impresa opera nel settore delle costruzioni edili, civili, industriali e delle opere pubbliche.</p>
                    <p>In questi anni l'attività dell'Azienda ha mantenuto lo stesso entusiasmo per il proprio lavoro cercando di seguire l'evoluzione dei tempi legati al settore immobiliare e delle costruzioni in genere.</p>
                    <p><strong>La nostra azienda si occupa direttamente della progettazione, costruzione e commercializzazione degli immobili, fornendo un servizio a 360° e garantendo così un notevole risparmio in termini di costi per i clienti.</strong></p>
                    <p>L'impresa, la cui sede è situata in Agropoli in provincia di Salerno, opera su tutto il territorio con grande competenza e professionalità. La qualità delle strutture realizzate e delle tecniche utilizzate in cantiere hanno permesso di ideare e conseguire progetti con uno standard di qualità sempre maggiore. Basandosi proprio su questi requisiti l'impresa è riuscita a raggiungere obiettivi fondamentali come la soddisfazione di ogni cliente.</p>
                    <p>L'impresa edile è costantemente affiancata da uno <strong>studio tecnico</strong> composto da tecnici professionisti di comprovata esperienza. Nel corso degli anni insieme all'impresa si è dunque valorizzato un team di professionisti che si occupa personalmente di tutte le fasi necessarie che vanno dalla progettazione preliminare a quella esecutiva, passando per la direzione lavori, il coordinamento della sicurezza, la progettazione strutturale e il collaudo.</p>
                    <p>Naviga il sito per leggere le schede di dettaglio degli immobili in vendita o in affitto oppure contattaci direttamente per richiedere informazioni e/o consulenze tecniche.</p>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- END OUR COMPANY -->

<!-- CONTACT -->
<section class="section" id="contact">
    <div class="container">
        <div class="row">
            <div class="jt_col-col-md-12 text-center voffset100">
                <h3 class="title main voffset100">CONTATTI</h3>
                <h3 class="title fz20 voffset0">VUOI UNA CONSULENZA?</h3>
                <h3 class="title primary fz50">+39 0974 824945</h3>
            </div>
            <div class="jt_col-col-md-12 voffset100">
                <form id="contactform" action="<?php echo $this->createUrl('/site/mail') ?>" method="post">
                    <div class="row text-center contact-form">
                        <div class="jt_col col-md-4">
                            <input id="name" name="name" type="text" class="required form-control" placeholder="Nome">
                        </div>
                        <div class="jt_col col-md-4">
                            <input id="email" name="email" type="text" class="required form-control" placeholder="Email">
                        </div>
                        <div class="jt_col col-md-4">
                            <input id="phone" name="phone" type="text" class="form-control" placeholder="Telefono">
                        </div>
                        <div class="jt_col col-md-12">
                            <textarea id="message" name="message" class="required form-control" placeholder="Messaggio"></textarea>
                        </div>
                        <div class="jt_col col-md-4 col-md-offset-4"><button type="submit" class="button fill">INVIA</button></div>
                        <div class="jt_col col-md-4 col-md-offset-4">
                            <div class="formSent success"><strong>Messaggio inviato con successo.</strong> Grazie per averci contattato!</div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="jt_col-col-md-12 voffset100 text-center">
                <h4 class="title fz16">ING. ANGELO ERRICO</h4>
                <h4 class="title fz16">via Difesa 6, Agropoli (SA)</h4>
                <h4 class="title fz16">TEL\FAX 0974 824945 | P.IVA 48789562</h4>
                <h4 class="title fz16 primary voffset60">APPALTITEMAIMPIANTI@LIBERO.IT</h4>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    .owl-dots{
        display: none !important;
    }
</style>