<!-- TEAM -->
<section id="team" class="section">
    <div class="container">
        <div class="row">
            <div class="jt_col col-md-8 jt_col col-md-offset-2 text-center">
                <h2 class="title main">TEAM</h2>
            </div>
        </div>
        <div class="row voffset100">
            <div class="col-sm-3">
                <div class="tmb-standar">
                    <div class="tmb-data">
                        <h3 class="tmb-title">ANGELO ERRICO</h3>
                        <h4 class="tmb-position">INGEGNERE</h4>
                        <!--<div class="tmb-links">
                            <a href="#"><i class="icon fa fa-facebook"></i></a>
                            <a href="#"><i class="icon fa fa-twitter"></i></a>
                            <a href="#"><i class="icon fa fa-google-plus"></i></a>
                            <a href="#"><i class="icon fa fa-pinterest"></i></a>
                            <a href="#"><i class="icon fa fa-instagram"></i></a>
                        </div>-->
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="tmb-standar">
                    <div class="tmb-data">
                        <h3 class="tmb-title">ELIA ELISEO<br> ERRICO</h3>
                        <h4 class="tmb-position">GEOMETRA</h4>
                        <!--<div class="tmb-links">
                            <a href="#"><i class="icon fa fa-facebook"></i></a>
                            <a href="#"><i class="icon fa fa-twitter"></i></a>
                            <a href="#"><i class="icon fa fa-google-plus"></i></a>
                            <a href="#"><i class="icon fa fa-pinterest"></i></a>
                            <a href="#"><i class="icon fa fa-instagram"></i></a>
                        </div>-->
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="tmb-standar">
                    <div class="tmb-data">
                        <h3 class="tmb-title">GIOVANNI INVERSO</h3>
                        <h4 class="tmb-position">GEOMETRA</h4>
                        <!--<div class="tmb-links">
                            <a href="#"><i class="icon fa fa-facebook"></i></a>
                            <a href="#"><i class="icon fa fa-twitter"></i></a>
                            <a href="#"><i class="icon fa fa-google-plus"></i></a>
                            <a href="#"><i class="icon fa fa-pinterest"></i></a>
                            <a href="#"><i class="icon fa fa-instagram"></i></a>
                        </div>-->
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="tmb-standar">
                    <div class="tmb-data">
                        <h3 class="tmb-title">Gennaro Celso</h3>
                        <h4 class="tmb-position">INGEGNERE</h4>
                        <!--<div class="tmb-links">
                            <a href="#"><i class="icon fa fa-facebook"></i></a>
                            <a href="#"><i class="icon fa fa-twitter"></i></a>
                            <a href="#"><i class="icon fa fa-google-plus"></i></a>
                            <a href="#"><i class="icon fa fa-pinterest"></i></a>
                            <a href="#"><i class="icon fa fa-instagram"></i></a>
                        </div>-->
                    </div>
                </div>
            </div>
<!--            <div class="col-sm-3">
                <div class="tmb-standar">
                    <div class="tmb-data">
                        <h3 class="tmb-title">Teresa Passaro</h3>
                        <h4 class="tmb-position">SEGRETERIA</h4>
                        <div class="tmb-links">
                            <a href="#"><i class="icon fa fa-facebook"></i></a>
                            <a href="#"><i class="icon fa fa-twitter"></i></a>
                            <a href="#"><i class="icon fa fa-google-plus"></i></a>
                            <a href="#"><i class="icon fa fa-pinterest"></i></a>
                            <a href="#"><i class="icon fa fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="tmb-standar">
                    <div class="tmb-data">
                        <h3 class="tmb-title">Maria A.<br>PASSARO</h3>
                        <h4 class="tmb-position">ACCOUNT</h4>
                        <div class="tmb-links">
                            <a href="#"><i class="icon fa fa-facebook"></i></a>
                            <a href="#"><i class="icon fa fa-twitter"></i></a>
                            <a href="#"><i class="icon fa fa-google-plus"></i></a>
                            <a href="#"><i class="icon fa fa-pinterest"></i></a>
                            <a href="#"><i class="icon fa fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
    </div>
</section>
<!-- END TEAM --> 