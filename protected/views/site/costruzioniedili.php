<div class="page-header single concrete">
    <div class="title light">
        <h1 class="light fz50">COSTRUZIONI EDILI</h1>
        <h2 class="light fz24">Costruzioni edili civili e grandi opere</h2>
    </div>
</div>

<section class="blog single">
    <span class="prev-post"><a href="<?php echo $this->createUrl('/catasto'); ?>"> Indietro</a></span>
    <span class="next-post"><a href="<?php echo $this->createUrl('/ristrutturazioni'); ?>">Avanti > </a></span>
    <div class="container">
        <article class="post">
            <div class="row">
                 <div class="col-sm-10 col-sm-offset-1 post-image">
                    <div class="generic-carousel" data-animation-out="slideOutRight" data-animation-in="slideInLeft" data-dots="false">
                        <div class="item">
                            <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/minislide/costruzioni_1.jpg" alt="Costruzioni Edili" class="img-responsive">
                        </div>
                        <div class="item">
                            <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/minislide/costruzioni_2.jpg" alt="Grandi lavori" class="img-responsive">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <h2 class="title fz26 upper">COSTRUZIONI EDILI, CIVILI E GRANDI LAVORI</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 post-content">
                    <div class="text">
                        <p>Realizziamo <strong>case nuove per civile abitazione</strong> sia su nostra iniziativa imprenditoriale sia su commissione pubblica o privata.</p>
                        <p>L’edilizia residenziale costituisce il nostro maggior impiego, tuttavia siamo costantemente impegnati anche nella realizzazione di strutture commerciali, industriali e di opere pubbliche.</p>
                        <p>Nella realizzazione di ogni nostra opera dedichiamo particolare attenzione alla scelta dei materiali, garantiamo un assiduo controllo nelle varie fasi del lavoro, curando anche il più piccolo dettaglio ed operiamo nel rispetto delle norme di sicurezza sul luogo di lavoro.</p>
                        <p>Dedichiamo inoltre un’attenzione speciale al tema <strong>dell’eco-sostenibilità e dell’efficienza energetica</strong>, due elementi fondamentali che permettono non solo di risparmiare sulle spese di energia e manutenzione, ma garantiscono anche un’alta qualità della vita e la tutela dell’ambiente che ci circonda. L’edilizia sostenibile impiega meno risorse naturali, produce un minore impatto sull’ambiente (nell’aria, nell’acqua, sul suolo) e garantisce un maggiore comfort abitativo rispetto all’edilizia convenzionale. Prestiamo inoltre attenzione all’utilizzo di materiali da costruzione privi di sostanze chimiche tossiche e nocive per l’uomo.</p>
                        <p>Vuoi realizzare la tua casa? Richiedi un preventivo gratuito. Lo studio tecnico si occuperà delle redazione del computo metrico e della stima dei costi da sostenere per la sua realizzazione.</p>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>