<div class="page-header single concrete">
    <div class="title light">
        <h1 class="light fz50">STUDIO TECNICO</h1>
        <h2 class="light fz24">CATASTO</h2>
    </div>
</div>

<section class="blog single">
    <span class="prev-post"><a href="<?php echo $this->createUrl('/calcoli-strutturali'); ?>"> Indietro</a></span>
    <span class="next-post"><a href="<?php echo $this->createUrl('/costruzioni-edili'); ?>">Avanti > </a></span>
    <div class="container">
        <article class="post">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 post-image">
                    <div class="generic-carousel" data-animation-out="slideOutRight" data-animation-in="slideInLeft" data-dots="false">
                        <div class="item">
                            <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/minislide/studio_1.jpg" alt="Catasto" class="img-responsive">
                        </div>
                        <div class="item">
                            <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/minislide/studio_2.jpg" alt="Catasto" class="img-responsive">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <h2 class="title fz26 upper">IL CATASTO</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 post-content">
                    <div class="text">
                        <p>Il Catasto è costituito dall'insieme di documenti, mappe ed atti che elencano e descrivono i beni immobili, con l'indicazione del luogo e del confine, con il nome dei loro possessori e le relative rendite, sulle quali vengono poi determinate tasse e imposte.</p>
                        <p>Il catasto vigente in Italia è, come recita il primo articolo della sua legge istitutiva, "geometrico", particellare e non "probatorio". Esso ha una funzione esclusivamente fiscale, serve cioè per accertare in modo uniforme il reddito imponibile sul quale verranno calcolate le tasse e le imposte sui beni immobili.</p>
                        <strong>Lo studio tecnico fornisce  un’assistenza a 360 gradi per la compilazione e il disbrigo delle pratiche catastali, fornendo ai clienti un valido supporto per districarsi in una materia complessa e in continua evoluzione.</strong><br>
                        <br>
                        Le pratiche catastali vengono svolte in maniera completa, a partire dal rilievo del fabbricato fino alla redazione delle planimetrie e dei modelli necessari per l’attribuzione della rendita catastale.<br>
                        <p>Oltre al servizio dedicato agli immobili di nuova costruzione, si svolgono anche tutte le pratiche di accatastamento in seguito a variazione, ad esempio dopo frazionamenti o ristrutturazioni.</p>
                        Visura per immobile, per soggetto o per indirizzo<br>
                        Visure ipotecarie (registri immobiliari)<br>
                        Planimetria catastale<br>
                        Estratto di mappa<br>
                        Elaborato planimetrico<br>
                        Elenco immobili<br>
                        Check-up immobile<br>
                        Pratiche DOCFA (variazioni di destinazione d’uso, fusioni, frazionamenti, variazioni, accatastamenti, ecc.)<br>
                        Successioni di morte e volture catastali<br>
                        Correzioni di errori al catasto terreni e urbano<br>
                        <br><br>
                        Hai bisogno di una consulenza tecnica? Contattaci! Il nostro staff sarà felice di aiutarti.
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>