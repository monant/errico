<div class="page-header single concrete">
    <div class="title light">
        <h1 class="light fz50">STUDIO TECNICO</h1>
    </div>
</div>

<section class="blog single">
    <span class="prev-post"><a href="<?php echo $this->createUrl('/consulenze-tecniche'); ?>"> Indietro</a></span>
    <span class="next-post"><a href="<?php echo $this->createUrl('/calcoli-strutturali'); ?>">Avanti > </a></span>
    <div class="container">
        <article class="post">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 post-image">
                    <div class="generic-carousel" data-animation-out="slideOutRight" data-animation-in="slideInLeft" data-dots="false">
                        <div class="item">
                            <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/minislide/studio_1.jpg" alt="Studio Tecnico" class="img-responsive">
                        </div>
                        <div class="item">
                            <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/minislide/studio_2.jpg" alt="Studio Tecnico" class="img-responsive">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <h2 class="title fz26 upper">STUDIO TECNICO</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 post-content">
                    <div class="text">
                        <p>Lo studio tecnico, a disposizione sia dell’impresa di società sia dei clienti terzi, è formato da un team di ingegneri e geometri con decennale esperienza nel campo dell’edilizia e dell’ingegneria in generale.</p>
                        <p>Lo studio svolge attività di progettazione edilizia (nuove costruzioni, ristrutturazioni, recupero di immobili), riqualificazione energetica di immobili con diagnosi energetica, certificazione energetica (APE), pratiche per la detrazione del 50% e 65%, gestione immobiliare, studi di fattibilità dell’opera, coordinatore della sicurezza nei cantieri, direzione lavori, calcoli strutturali e collaudi statici, rilievi topografici ed operazioni catastali (accatastamenti, frazionamenti, piano quotati, picchettamenti).</p><br>
                        <p>Di seguito un elenco non esaustivo dei servizi offerti dallo studio tecnico:</p><br>

                        <strong>RICHIESTE PER AUTORIZZAZIONI EDILIZIE</strong><br>
                        Richiesta di permesso di costruire<br>
                        S.C.I.A. Segnalazione Certificata di Inizio Attività<br>
                        C.I.L. Certificazione Inizio Lavori<br>
                        C.I.L.A. Certificazione Inizio Lavori Asseverata<br>
                        Manutenzione ordinaria e straordinaria<br>
                        Condoni e sanatorie<br>
                        Svincoli idrogeologici<br>
                        Recupero sottotetti<br>
                        Frazionamenti immobiliari<br>
                        Cambi di destinazione d’uso<br>
                        Segnalazione Certificata di Agibilità (ex. Certificato di Agibilità)<br>
                        <br>
                        <strong>PROGETTAZIONE ED ESECUZIONE LAVORI</strong><br>
                        Progettazione architettonica<br>
                        Direzione dei lavori<br>
                        Computi metrici<br>
                        Capitolati d’appalto<br>
                        Contratti d’appalto<br>
                        <br>
                        <strong>PROGETTAZIONE STRUTTURALE E COLLAUDI STATICI</strong><br>
                        Progettazione strutturale – Autorizzazione/deposito sismico<br>
                        Direzione dei lavori strutturali<br>
                        Collaudi<br>
                        Idoneità Statica<br>
                        Idoneità Sismica<br>
                        <br>
                        <strong>SICUREZZA TUTELA DELLA SALUTE E DELLA SICUREZZA NEI LUOGHI DI LAVORO E NEI CANTIERI (D. LGS 81/2008)</strong><br>
                        PSC Piano di Sicurezza e Coordinamento<br>
                        POS Piano Operativo di Sicurezza<br>
                        DVR Documento di Valutazione dei Rischi per le imprese <br>
                        DUVRI Documento Unico di Valutazione dei Rischi per le imprese<br>
                        PIMUS Piano di Montaggio, Uso e Smontaggio dei Ponteggi<br>
                        PSS Piano Sostitutivo di Sicurezza<br>
                        Coordinamento della sicurezza in fase di progettazione e/o in fase di esecuzione<br>
                        <br>
                        <strong>CONSULENZA IN MATERIA DI RISPARMIO ENERGETICO</strong><br>
                        APE Attestato di Prestazione Energetica<br>
                        Diagnosi energetiche<br>
                        Relazione sullo stato energetico di appartamenti ed edifici e progettazione di interventi di riqualificazione energetica<br>
                        Adempimenti tecnici per l’ottenimento delle agevolazioni fiscali del 55% per interventi di riqualificazione energetica (es. per interventi di coibentazione termica, sostituzione di finestre comprensive di infissi, impianti di riscaldamento, impianti di climatizzazione estiva, caldaie, fonti di energia rinnovabile, ecc.).<br>
                        <br>
                        <strong>CONSULENZE VARIE E ASSISTENZE</strong><br>
                        Stima del valore di immobili<br>
                        Perizie tecniche e giurate<br>
                        Consulenza e assistenza tecnica alla vendita ed acquisto di immobili<br>
                        Consulenze notarili (compravendita, divisione, donazione)<br>
                        <br>
                        <strong>AGENZIA DEL TERRITORIO – CATASTO</strong><br>
                        Visure catastali ed estratti di mappa catastale terreni. Questo servizio può essere erogato anche a distanza (visure catastali) in tutta Italia facendovi risparmiare molto tempo.<br>
                        Estratti di mappa catastale urbana
                        Ricerche e visure in Conservatoria (Registri Immobiliari)<br>
                        Redazione pratiche DOCFA (frazionamenti, variazioni, nuovi accatastamenti, fusioni, ecc)<br>
                        Successioni di morte e volture catastali<br>
                        Correzioni di errori al catasto terreni e urbano<br>
                        <br><br>
                        Hai bisogno di una consulenza tecnica? Contattaci! Il nostro staff sarà felice di aiutarti.<br>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>