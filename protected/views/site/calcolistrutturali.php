<div class="page-header single concrete">
    <div class="title light">
        <h1 class="light fz50">STUDIO TECNICO</h1>
        <h2 class="light fz24">CALCOLI STRUTTURALI E COLLAUDI STATICI</h2>
    </div>
</div>

<section class="blog single">
    <span class="prev-post"><a href="<?php echo $this->createUrl('/studio-tecnico'); ?>"> Indietro</a></span>
    <span class="next-post"><a href="<?php echo $this->createUrl('/catasto'); ?>">Avanti > </a></span>
    <div class="container">
        <article class="post">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 post-image">
                    <div class="generic-carousel" data-animation-out="slideOutRight" data-animation-in="slideInLeft" data-dots="false">
                        <div class="item">
                            <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/minislide/calcoli_1.jpg" alt="Calcoli Strutturali" class="img-responsive">
                        </div>
                        <div class="item">
                            <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/minislide/calcoli_2.jpg" alt="Calcoli Strutturali" class="img-responsive">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <h2 class="title fz26 upper">CALCOLI STRUTTURALI E COLLAUDI STATICI</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 post-content">
                    <div class="text">
                        <p>Lo studio tecnico cura, nell'ambito specifico dell'Ingegneria Strutturale, i servizi di consulenza per la progettazione ed il calcolo delle opere in cemento armato, legno, muratura ed acciaio. Serietà, professionalità e cura del singolo dettaglio sono la base predominante su cui verte questo particolare settore.</p>
                        <p>E' gestita la progettazione preliminare, definitiva ed esecutiva di costruzioni civili ed industriali, con cura nell'intero iter progettuale, mediante una costante attenzione all'aspetto strutturale fino alla redazione degli esecutivi.</p><br>
                        <br>
                        <strong>1. OPERE OGGETTE A DEPOSITO</strong><br>
                        Tutti i lavori di edifici o manufatti con strutture composte da cemento armato normale e/o precompresso, oppure con strutture metalliche sono soggette all’obbligo di denuncia delle opere. Sono escluse da tale obbligo:<br>
                        - le opere in cui compaiono soltanto strutture in conglomerato cementizio non armato, strutture murarie in laterizio, strutture in legno o simili (ad es. muri di sostegno in cls. a gravità, opere in legno lamellare etc.);<br>
                        - le opere di lieve entità statica (rif. a Circolare Ministero LLPP. N° 11951 d.d. 14/2/74: “Sono quindi escluse dall’applicazione dell’art. 4 della legge, oltre le membrature singole, anche gli elementi costruttivi in c.a. che assolvono una limitata importanza nel contesto statico dell’opera”);<br>
                        - le opere costruite per conto di enti pubblici aventi un ufficio tecnico con a capo un ingegnere.<br><br>

                        <strong>2. RICEZIONE DELLA DENUNCIA</strong><br>
                        Il COSTRUTTORE, prima dell’inizio dei lavori, deve presentare, ai sensi dell’art. 4 della Legge 1086/71, la denuncia delle opere in c.a., c.a.p. e a struttura metallica che intende realizzare.<br>
                        La denuncia costituisce l’atto cui e’ subordinato l’avvio della procedura e deve contenere:<br>
                        - i nomi ed i recapiti del Committente, del Progettista delle strutture, del Direttore dei Lavori e del Costruttore stesso;<br>
                        - il Comune interessato alla costruzione, la località con il numero di particella e il relativo Comune Catastale, nonché il tipo di intervento e di opera (es. ristrutturazione e/o ampliamento, sopraelevazione etc.; edificio di abitazione oppure ponte, strada etc.)<br>
                        Alla denuncia dei lavori (in marca da bollo), in duplice esemplare e a firma del Costruttore, dovranno essere allegati:<br>
                        1. accettazione dell’incarico del Collaudatore, firmato e timbrato;<br>
                        2. relazione tecnica illustrativa, firmata e timbrata dal progettista delle strutture e dal Direttore dei Lavori, dalla quale risultino le caratteristiche, le qualità e le dosature dei materiali che verranno impiegati nella costruzione;<br>
                        3. relazione di calcolo, con relativi disegni delle strutture, firmata e timbrata dal progettista delle strutture;<br>
                        4. progetto strutturale dell'opera, completo di tutte le tavole relative agli elementi strutturali, timbrate e firmate dal Progettista;<br>
                        5. progetto architettonico dell’opera, completo di planimetria con estratto mappa, piante, sezioni e prospetti firmati e timbrati dal Progettista;<br>
                        <br>
                        La denuncia e gli allegati devono essere presentati su fogli di formato A4 o piegati nella stessa misura. Spetta all’Ufficio preposto alla ricezione controllare che la denuncia contenga gli allegati in duplice esemplare. Tale controllo è di natura prettamente formale. Al momento del ricevimento della denuncia si registrano i dati attribuendo un numero progressivo alla pratica; tale numerazione del fascicolo dovrà essere richiamata in tutti i successivi atti presentati. Al momento della presentazione, si restituisce un esemplare della documentazione presentata con apposto il timbro attestante l’avvenuto deposito.<br>
                        <br>
                        Nel caso di strutture prefabbricate di rilevante entità, di cui vengono eseguite specifiche relazioni di calcolo, va effettuata una denuncia separata, il cui numero è dato da codice/lettera in cui codice corrisponde al numero della denuncia dell’opera principale e lettera viene assegnata progressivamente a ciascuna relazione separata. Per questi casi la Relazione Illustrativa dei Materiali diventa vincolante.<br>
                        <br>
                        In altri casi è possibile avere relazioni di calcolo che riguardino solo parte della struttura o dettagli specifici, firmate da un tecnico diverso dal progettista incaricato. Esse vanno allegate alla denuncia, controfirmate dal progettista delle strutture individuato nella denuncia delle opere.<br><br>
                        Nel caso di varianti e/o integrazioni si segue la stessa procedura della denuncia originaria (in marca da bollo), con la dizione “VARIANTE e/o INTEGRAZIONE” e la citazione del numero della denuncia precedentemente fatta. Gli elaborati vanno controfirmati dal progettista delle strutture indicato nella denuncia iniziale. Il tutto va presentato prima di iniziare i relativi lavori.<br>
                        <br>
                        <strong>3. RICEZIONE DELLA DOCUMENTAZIONE A STRUTTURA UTLIMATA</strong><br>
                        Il DIRETTORE LAVORI, a struttura ultimata deve redigere e consegnare:<br>
                        <br>
                        1. la Dichiarazione di Fine Lavori, firmata e timbrata;<br>
                        2. due copie della Relazione a Struttura Ultimata, firmate e timbrate, da depositare entro 60 giorni dalla data dichiarata come fine lavori. A questa vanno allegati l’originale e la fotocopia dei certificati delle prove sui materiali (ferro e calcestruzzo), emessi dai laboratori autorizzati. Nel caso di strutture prefabbricate vanno allegati i relativi Certificati d’origine.<br>
                        <br>
                        <strong>4.RICEZIONE DELLA NOMINA DEL COLLAUDATORE</strong><br>
                        La Nomina del Collaudatore va presentata dal committente entro 60 giorni dal termine dei lavori. Si consiglia comunque di presentare la Nomina contestualmente alla denuncia dei lavori. Nel caso di lavori in economia (Committente e Costruttore coincidono) il collaudatore deve essere scelto dal Committente fra una terna di professionisti nominati dall’Ordine degli Ingegneri o degli Architetti. Copia della lettera di nomina della terna va allegata alla Nomina del Collaudatore. Se la nomina non è presentata assieme alla denuncia dei lavori, la lettera deve essere presentata in allegato a quest’ultima. Nel registro c.a. verrà riportato anche il nominativo del collaudatore prescelto e la data di deposito della nomina.<br>
                        <br>
                        <strong>5. Ricezione del certificato di collaudo statico</strong><br>
                        Tutte le opere denunciate devono essere collaudate. Il Collaudatore deve consegnare 2 esemplari originali firmati, datati e timbrati del certificato di collaudo, in bollo. Occorre verificare che tale certificato sia stato redatto dal Collaudatore prescelto dal Committente. Un esemplare viene restituito al Collaudatore con apposito timbro di avvenuto deposito. Nel registro c.a. verrà riportata la data di ricevimento del certificato. In tutti i casi, il collaudo deve essere eseguito da un Ingegnere o Architetto iscritto all’Albo da almeno 10 anni, e che non sia intervenuto in nessun modo nella progettazione, direzione ed esecuzione dell’opera. Ciò va dichiarato dal Collaudatore, e la dichiarazione è soggetta alla responsabilità del Collaudatore stesso.<br>
                        <br>
                        E’ possibile consegnare il certificato di collaudo insieme alla Nomina del Collaudatore (se non già depositata insieme alla denuncia dei lavori), alla dichiarazione di fine lavori e alla Relazione a Struttura Ultimata.<br>
                        <br><br>
                        Hai bisogno di una consulenza tecnica? Contattaci! Il nostro staff sarà felice di aiutarti.
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>