<!-- Services -->
<section id="services" class="section">
    <div class="container">
        <div class="row">
            <div class="jt_col col-md-8 jt_col col-md-offset-2 text-center">
                <h2 class="title main">COSA FACCIAMO</h2>
            </div>
        </div>
        <div class="row voffset100">
            <div class="col-sm-4">
                <div class="wwd-block">
                    <a href="<?php echo $this->createUrl('/costruzioni-edili'); ?>"><img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/cf_1.jpg" alt=""></a>
                    <div class="wwd-data">
                        <h3 class="wwd-title">Costruzioni edili</h3>
                        <a href="<?php echo $this->createUrl('/costruzioni-edili'); ?>" class="wwd-link">Leggi altro...</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="wwd-block">
                    <a href="<?php echo $this->createUrl('/ristrutturazioni'); ?>"><img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/cf_2.jpg" alt=""></a>
                    <div class="wwd-data">
                        <h3 class="wwd-title">Ristrutturazioni</h3>
                        <a href="<?php echo $this->createUrl('/ristrutturazioni'); ?>" class="wwd-link">Leggi altro..</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="wwd-block">
                    <a href="<?php echo $this->createUrl('/consulenze-tecniche'); ?>"><img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/cf_3.jpg" alt=""></a>
                    <div class="wwd-data">
                        <h3 class="wwd-title">Consulenze Tecniche</h3>
                        <a href="<?php echo $this->createUrl('/consulenze-tecniche'); ?>" class="wwd-link">Leggi altro..</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="wwd-block">
                    <a href="<?php echo $this->createUrl('/studio-tecnico'); ?>"><img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/cf_4.jpg" alt=""></a>
                    <div class="wwd-data">
                        <h3 class="wwd-title">Studio Tecnico<br></h3>
                        <a href="<?php echo $this->createUrl('/studio-tecnico'); ?>" class="wwd-link">Leggi altro..</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="wwd-block">
                    <a href="<?php echo $this->createUrl('/calcoli-strutturali'); ?>"><img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/cf_5.jpg" alt=""></a>
                    <div class="wwd-data">
                        <h3 class="wwd-title">Calcoli Strutturali</h3>
                        <a href="<?php echo $this->createUrl('/calcoli-strutturali'); ?>" class="wwd-link">Leggi altro..</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="wwd-block">
                    <a href="<?php echo $this->createUrl('/catasto'); ?>"><img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/cf_6.jpg" alt=""></a>
                    <div class="wwd-data">
                        <h3 class="wwd-title">Catasto</h3>
                        <a href="<?php echo $this->createUrl('/catasto'); ?>" class="wwd-link">Leggi altro..</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END Services -->