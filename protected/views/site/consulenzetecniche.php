<div class="page-header single concrete">
    <div class="title light">
        <h1 class="light fz50">CONSULENZE TECNICHE</h1>
    </div>
</div>

<section class="blog single">
    <span class="prev-post"><a href="<?php echo $this->createUrl('/ristrutturazioni'); ?>"> Indietro</a></span>
    <span class="next-post"><a href="<?php echo $this->createUrl('/studio-tecnico'); ?>">Avanti > </a></span>
    <div class="container">
        <article class="post">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 post-image">
                    <div class="generic-carousel" data-animation-out="slideOutRight" data-animation-in="slideInLeft" data-dots="false">
                        <div class="item">
                            <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/minislide/consulenze_1.jpg" alt="Consulenze Tecniche Edili" class="img-responsive">
                        </div>
                        <div class="item">
                            <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/minislide/consulenze_2.jpg" alt="Consulenze Tecniche Edili" class="img-responsive">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <h2 class="title fz26 upper">CONSULENZE TECNICHE</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 post-content">
                    <div class="text">
                        <p>Il cliente interessato può richiedere servizi di consulenza e di sopralluoghi gratuiti per quanto riguarda lavori di realizzazione ex-novo di opere edili/civili, ristrutturazioni più o meno rilevanti, consigli, pareri e tutto ciò che riguarda in generale il mondo dell’edilizia.</p><br>
                        <p>Uno staff di esperti composto da ingegneri e geometri con esperienza comprovata sarà a completa disposizione per soddisfare tutte le richieste e le esigenze della clientela.</p><br>
                        <p>Hai bisogno di una consulenza tecnica o di un semplice consiglio/parere? Contattaci! Il nostro staff è a disposizione per lavori con specifiche richieste presentate dal cliente, proponendo le soluzioni migliori per le tue esigenze.</p>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>