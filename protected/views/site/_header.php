<header id="header">
    <div class="container">
        <div class="row info-bar">
            <div class="col-sm-6">Tel. 0974 824945 - Cel. 335 8725255</div>
            <div class="col-sm-6 text-right">
                <a href="#" class="social-icon"><i class="icon fa fa-facebook"></i></a>
                <a href="#" class="social-icon"><i class="icon fa fa-youtube"></i></a>
            </div>
        </div>
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                <i class="icon fa fa-bars"></i>
            </button>
            <a class="navbar-brand normal" href="#home"><img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/logo.png" alt="uniform"/></a>
        </div>
        <nav class="collapse navbar-collapse navbar-right navbar-main-collapse">
            <ul id="nav" class="nav navbar-nav navigation">
                <li class="page-scroll menu-item"><a href="#home-slider">Home</a></li>
                <li class="page-scroll menu-item"><a href="#services">SERVIZI</a></li>
                <li class="page-scroll menu-item"><a href="<?php echo $this->createUrl('/immobili') ?>">IMMOBILI</a></li>
<!--                <li class="page-scroll menu-item"><a href="#projects">PROGETTI</a></li>-->
                <li class="page-scroll menu-item"><a href="#team">TEAM</a></li>
                <li class="page-scroll menu-item"><a href="#company">AZIENDA</a></li>
                <li class="page-scroll menu-item"><a href="#contact">CONTATTI</a></li>
            </ul>
        </nav>
    </div>
</header> 