<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-3">
                <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/logo-footer.png" alt="" class="img-responsive">
                <div class="voffset30"></div>
                <p class="block-title">info</p>
                <p class="subtitle light">Per qualsiasi tipo di informazioni non esitare a contattarci.</p>
            </div>
            <div class="col-md-6 col-lg-3">
                <p class="block-title">contatti</p>
                <ul class="contact-info">
                    <li class="subtitle light"><i class="icon fa fa-building-o"></i>via Difesa 6, Agropoli SA</li>
                    <li class="subtitle light"><i class="icon fa fa-phone"></i>+39 0974 824945</li>
                    <li class="subtitle light"><i class="icon fa fa-phone"></i>+39 335 8725255</li>
                    <li class="subtitle light"><i class="icon fa fa-envelope-o"></i>appaltitemaimpianti@libero.it</li>
                    <li class="subtitle light"><i class="icon fa fa-clock-o"></i>8:00 - 19:00</li>
                </ul>
            </div>
            <div class="col-md-6 col-lg-3">
                <p class="block-title">seguici su</p>
                <div class="row">
                    <div class="col-xs-4">
                        <a href="" class="social-icon"><i class="icon fa fa-facebook"></i></a>
                    </div>
                    <div class="col-xs-4">
                        <a href="" class="social-icon"><i class="icon fa fa-youtube"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="footer-posts">
                    <p class="block-title">Contatti diretti</p>
                    <div class="footer-post">
                        <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/footico_1.png" alt="Angelo Errico" style="width:56px; height:56px;">
                        <div class="post-data">
                            <p class="subtitle fz12">Ing.</p>
                            <p class="subtitle light"><a href="#">ANGELO ERRICO</a></p>
                            <p class="subtitle fz14" style="color:white;">335 8725255</p>
                        </div>
                    </div>
                    <div class="footer-post">
                        <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/footico_2.png" alt="Gennaro Celso" style="width:56px; height:56px;">
                        <div class="post-data">
                            <p class="subtitle fz12">Ing.</p>
                            <p class="subtitle light"><a href="#">GENNARO CELSO</a></p>
                            <p class="subtitle fz14" style="color:white;">345 9211074</p>
                        </div>
                    </div>
                    <div class="footer-post">
                        <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/footico_3.png" alt="Giovanni Inverso" style="width:56px; height:56px;">
                        <div class="post-data">
                            <p class="subtitle fz12">Geom.</p>
                            <p class="subtitle light"><a href="#">GIOVANNI INVERSO</a></p>
                            <p class="subtitle fz14" style="color:white;">324 7472309</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a href="#top" class="scrolltop"><i class="fa fa-angle-up"></i></a>
</footer>