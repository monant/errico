<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="language" content="it">

        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Montserrat:400,700%7CDroid+Serif:400italic%7COpen+Sans:400,300,600'" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/uniform.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css" />
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyB7tDT6diNLxS3SZ-9QxxOck-gFpFTcYic"></script>
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <style type="text/css">
            .loader {
                position: fixed;
                width:100%;
                height: 100%;
                background-color: #fff;
                z-index: 9999;
            }
        </style>
    </head>
    <body class="overflowed" data-spy="scroll" data-target=".navbar-main-collapse">
        <!-- header begin -->
        <?php $this->renderPartial('/site/_headerpage') ?>
        <?php echo $content; ?>        
        <?php $this->renderPartial('/site/_map') ?>
        <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/js/jquery-1.11.3.min.js"></script>
        <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/js/jquery.hoverdir.js"></script>
        <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/js/bootstrap.min.js"></script>
        <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/js/imagesloaded.pkgd.min.js"></script>
        <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/js/isotope.pkgd.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
        <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/js/jquery.inview.min.js"></script>
        <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/js/jquery.mb.YTPlayer.min.js"></script>
        <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/js/jquery.validate.min.js"></script>
        <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/js/jquery.form.min.js"></script>
        <script src="<?php echo Yii::app()->getBaseUrl(true) ?>/js/default.js"></script>
        <?php $this->renderPartial('/site/_footer') ?>
    </body>
</html>
