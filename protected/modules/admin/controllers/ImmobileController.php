<?php

class ImmobileController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = 'main_crud';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'gallery', 'uploadgallery', 'deletegallery', 'sort', 'sortgallery'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionSort() {
        if (isset($_POST['items']) && is_array($_POST['items'])) {
            $i = 0;
            foreach ($_POST['items'] as $item) {
                $project = Immobile::model()->findByPk($item);
                $project->posizione = $i;
                $project->save(false);
                $i++;
            }
        }
    }
    
    public function actionSortgallery() {
        if (isset($_POST['items']) && is_array($_POST['items'])) {
            $i = 0;
            foreach ($_POST['items'] as $item) {
                $project = Gallery::model()->findByPk($item);
                $project->posizione = $i;
                $project->save(false);
                $i++;
            }
        }
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionGallery($id) {
        $model_gallery = new Gallery();
        $model = new Gallery('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Gallery']))
            $model->attributes = $_GET['Gallery'];
        $this->render('gallery', array(
            'model' => $this->loadModel($id),
            'gallery' => $model,
            'model_gallery' => $model_gallery
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Immobile;
        $model->eliminato = 0;
        $model->latitudine = '40.35';
        $model->longitudine = "14.983333";
        $model->posizione = $this->getLastPosition();

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

        if (isset($_POST['Immobile'])) {
            $model->attributes = $_POST['Immobile'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

        if (isset($_POST['Immobile'])) {
            $model->attributes = $_POST['Immobile'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {

            $m = $this->loadModel($id);
            $m->eliminato = 1;
            $m->save();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Manages all models.
     */
    public function actionIndex() {
        $model = new Immobile('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Immobile']))
            $model->attributes = $_GET['Immobile'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Immobile::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'immobile-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionUploadGallery() {
        $model = new Gallery();
        if (isset($_POST['Gallery'])) {
            $model->attributes = $_POST['Gallery'];
            $model->filename = CUploadedFile::getInstance($model, 'filename');
            $model->posizione = $this->getLastPositionGallery($model->id_immobile);
            if ($model->validate()) {
                $filename = uniqid() . '.' . $model->filename->extensionName;
                $model->filename->saveAs(Yii::app()->basePath . '/../uploads/gallery/' . $filename);
                $model->filename = $filename;
                $model->save(false);
            }
        }
        $this->redirect(array('gallery', 'id' => $model->id_immobile));
    }

    public function actionDeleteGallery($id) {
        $m = Gallery::model()->findByPk($id);
        if (isset($m)) {
            $id_immobile = $m->id_immobile;
            $m->delete();
            $this->redirect(array('gallery', 'id' => $id_immobile));
        }
    }

    public function getLastPosition() {
        $criteria = new CDbCriteria();
        $criteria->order = "posizione DESC";
        $criteria->limit = 1;
        $model = Immobile::model()->find($criteria);
        if (!$model)
            return 0;
        else
            return $model->posizione + 1;
    }
    
    public function getLastPositionGallery($id_immobile) {
        $criteria = new CDbCriteria();
        $criteria->order = "posizione DESC";
        $criteria->limit = 1;
        $criteria->condition = "id_immobile = $id_immobile";
        $model = Gallery::model()->find($criteria);
        if (!$model)
            return 0;
        else
            return $model->posizione + 1;
    }

}
