<?php
$this->breadcrumbs=array(
	'Immobiles',
);

$this->menu=array(
array('label'=>'Create Immobile','url'=>array('create')),
array('label'=>'Manage Immobile','url'=>array('admin')),
);
?>

<h1>Immobiles</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
