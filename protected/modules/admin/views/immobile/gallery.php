<?php
Yii::app()->clientScript->registerScriptFile('http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js');
$str_js = "
        var fixHelper = function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        };
 
        $('#roomgallery-grid table.items tbody').sortable({
            forcePlaceholderSize: true,
            forceHelperSize: true,
            items: 'tr',
            update : function () {
                serial = $('#roomgallery-grid table.items tbody').sortable('serialize', {key: 'items[]', attribute: 'class'});
                $.ajax({
                    'url': '" . $this->createUrl('/admin/immobile/sortgallery') . "',
                    'type': 'post',
                    'data': serial,
                    'success': function(data){
                    },
                    'error': function(request, status, error){
                        alert('We are unable to set the sort order at this time.  Please try again in a few minutes.');
                    }
                });
            },
            helper: fixHelper
        }).disableSelection();
    ";

Yii::app()->clientScript->registerScript('sortable-project', $str_js);
?>

<?php
$this->breadcrumbs = array(
    'Immobili' => array('index'),
    $model->id,
);

$this->menu = array(
    array('label' => 'Lista Immobili', 'url' => array('index')),
    array('label' => 'Nuovo Immobile', 'url' => array('create')),
    array('label' => 'Modifica Immobile', 'url' => array('update', 'id' => $model->id)),
    array('label' => "Torna all'immobile", 'url' => array('view', 'id' => $model->id)),
        //array('label' => 'Manage Immobile', 'url' => array('admin')),
);
?>

<h1>Vedi Galleria #<?php echo $model->id; ?></h1>

<?php
$this->widget('booster.widgets.TbGridView', array(
    'id' => 'roomgallery-grid',
    'dataProvider' => $gallery->search($model->id),
    'filter' => $gallery,
    'afterAjaxUpdate' => 'reInstallSortable',
    'rowCssClassExpression' => '"items[]_{$data->id}"',
    'columns' => array(
        array('name' => 'filename', 'value' => '$data->getImage()', 'filter' => '', 'type' => 'html'),
        //   'posizione',
        array(
            'class' => 'booster.widgets.TbButtonColumn',
            'template' => '{delete}',
            'deleteButtonUrl' => 'Yii::app()->controller->createUrl("immobile/deleteGallery", array("id" => $data->primaryKey))'
        ),
    ),
));
?>
<br/>
<hr/>
<br/>
<h3>Aggiungi immagine</h3>
<?php
$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
    'id' => 'roomgallery-form',
    'action' => $this->createUrl('uploadGallery'),
    'enableAjaxValidation' => false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>

<?php echo $form->errorSummary($model_gallery); ?>

<?php echo $form->hiddenField($model_gallery, 'id_immobile', array('class' => 'span5', 'value' => $model->id)); ?>

<?php echo $form->fileFieldGroup($model_gallery, 'filename', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5', 'maxlength' => 256)))); ?>


<div class="form-actions">
    <?php
    $this->widget('booster.widgets.TbButton', array(
        'buttonType' => 'submit',
        'context' => 'primary',
        'label' => $model->isNewRecord ? 'Aggiungi' : 'Aggiungi',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>

<style type="text/css">
    .max200{
        max-width: 100px; 
        max-height:100px
    }
</style>