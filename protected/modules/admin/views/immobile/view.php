<?php
$this->breadcrumbs = array(
    'Immobili' => array('index'),
    $model->id,
);

$this->menu = array(
    array('label' => 'Lista Immobili', 'url' => array('index')),
    array('label' => 'Nuovo Immobile', 'url' => array('create')),
    array('label' => 'Modifica Immobile', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Galleria', 'url' => array('gallery', 'id' => $model->id)),
    array('label' => 'Elimina Immobile', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Sei sicuro?')),
        //array('label' => 'Manage Immobile', 'url' => array('admin')),
);
?>

<h1>Vedi Immobile #<?php echo $model->id; ?></h1>

<?php
$this->widget('booster.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        array('name' => 'id_localita', 'value' => $model->getLocalita()),
        'tipologia',
        'indirizzo',
        'prezzo',
        'metratura',
        'piano',
        'locali',
        'bagno',
        'esterno',
        'ascensore',
        'riscaldamento',
        'giardino',
        'box',
        'classe_energetica',
        'realizzazione',
        'latitudine',
        'longitudine',        
        array('name' => 'pubblicato', 'value' => $model->pubblicato == 1 ? "SI" : "NO"),
  //      'posizione',
        array('name' => 'informazioni', 'type' => 'raw', 'value' => nl2br($model->informazioni)),
    //   'eliminato',
    ),
));
?>
