<style type="text/css">
    #map {
        height: 400px;
        width: 100%;
    }

    .no-padding .col-sm-9{
        padding-left: 0 !important;
    }

</style>
<?php
$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
    'id' => 'immobile-form',
    'enableAjaxValidation' => false,
        ));
?>

<p class="help-block">I campi con <span class="required">*</span> sono obbligatori.</p>

<?php echo $form->errorSummary($model); ?>

<?php
echo $form->dropDownListGroup($model, 'id_localita', array(
    'wrapperHtmlOptions' => array(
        'class' => 'col-sm-5',
    ),
    //'label' => 'Seleziona una località',
    'widgetOptions' => array(
        'data' => ['' => ''] + $model->getLocalitas(),
        'htmlOptions' => array(),
    )
));
?>

<?php echo $form->textFieldGroup($model, 'tipologia', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5', 'maxlength' => 256)))); ?>

<?php echo $form->textFieldGroup($model, 'indirizzo', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5', 'maxlength' => 256)))); ?>

<?php echo $form->textFieldGroup($model, 'prezzo', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5', 'maxlength' => 256)))); ?>

<?php echo $form->textAreaGroup($model, 'informazioni', array('widgetOptions' => array('htmlOptions' => array('rows' => 6, 'cols' => 50, 'class' => 'span8')))); ?>

<?php echo $form->textFieldGroup($model, 'metratura', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5', 'maxlength' => 256)))); ?>

<?php echo $form->textFieldGroup($model, 'piano', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5', 'maxlength' => 256)))); ?>

<?php echo $form->textFieldGroup($model, 'locali', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5', 'maxlength' => 256)))); ?>

<?php echo $form->textFieldGroup($model, 'bagno', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5', 'maxlength' => 256)))); ?>

<?php echo $form->textFieldGroup($model, 'esterno', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5', 'maxlength' => 256)))); ?>

<?php echo $form->textFieldGroup($model, 'ascensore', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5', 'maxlength' => 256)))); ?>

<?php echo $form->textFieldGroup($model, 'riscaldamento', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5', 'maxlength' => 256)))); ?>

<?php echo $form->textFieldGroup($model, 'giardino', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5', 'maxlength' => 256)))); ?>

<?php echo $form->textFieldGroup($model, 'box', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5', 'maxlength' => 256)))); ?>

<?php echo $form->textFieldGroup($model, 'classe_energetica', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5', 'maxlength' => 256)))); ?>

<?php echo $form->textFieldGroup($model, 'realizzazione', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5', 'maxlength' => 256)))); ?>

<?php echo $form->checkboxGroup($model, 'pubblicato', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5')))); ?>

<?php $form->textFieldGroup($model, 'posizione', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5')))); ?>    

<div id="map"></div>
<script>
    var map;
    var geocoder;
    var marker;
    function initMap() {
        var munic = {lat: <?= $model->latitudine ?>, lng: <?= $model->longitudine ?>};
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 14,
            center: munic,
            scrollwheel: false
        });
        marker = new google.maps.Marker({
            position: munic,
            map: map,
            draggable: true,
        });

        geocoder = new google.maps.Geocoder();

        // now attach the event
        google.maps.event.addListener(marker, 'dragend', function () {
            $('#Immobile_latitudine').val(marker.position.lat().toFixed(8));
            $('#Immobile_longitudine').val(marker.position.lng().toFixed(8));
        });
    }

    function onKeyUpAddress(e) {
        if (e.keyCode == 13) {
            geocode();

            e.preventDefault();
        }
    }

    function geocode() {
        var address = $('#indirizzo').val();

        geocoder.geocode({'address': address}, function (results, status) {
            if (status == 'OK') {
                map.setCenter(results[0].geometry.location);
                marker.setPosition(results[0].geometry.location);

                $('#Immobile_latitudine').val(marker.position.lat().toFixed(8));
                $('#Immobile_longitudine').val(marker.position.lng().toFixed(8));
            }
        });

    }

</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCX-97BldSXRSBsvVg5gC0D9MyDEmMFx_Y&callback=initMap">
</script>

<br/>
<div class="form-group no-padding">
    <div class="col-sm-9">
        <input class="span5 form-control" type="text" id="indirizzo" placeholder="Cerca indirizzo" onkeydown="return onKeyUpAddress(event);"/>    
    </div>
    <div class="col-sm-3">
        <input class="btn btn-success" type="button" onclick="geocode();" value="Cerca" />    
    </div>
    <div style="clear: both"></div>
</div>
<div class="clearfix"></div>

<div style="display: none">
    <?php echo $form->textFieldGroup($model, 'latitudine', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5', 'maxlength' => 256)))); ?>

    <?php echo $form->textFieldGroup($model, 'longitudine', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5', 'maxlength' => 256)))); ?>
</div>


<div class="form-actions">
    <?php
    $this->widget('booster.widgets.TbButton', array(
        'buttonType' => 'submit',
        'context' => 'primary',
        'label' => $model->isNewRecord ? 'Aggiungi' : 'Modifica',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
