<?php
Yii::app()->clientScript->registerScriptFile('http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js');
$str_js = "
        var fixHelper = function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        };
 
        $('#immobile-grid table.items tbody').sortable({
            forcePlaceholderSize: true,
            forceHelperSize: true,
            items: 'tr',
            update : function () {
                serial = $('#immobile-grid table.items tbody').sortable('serialize', {key: 'items[]', attribute: 'class'});
                $.ajax({
                    'url': '" . $this->createUrl('/admin/immobile/sort') . "',
                    'type': 'post',
                    'data': serial,
                    'success': function(data){
                    },
                    'error': function(request, status, error){
                        alert('We are unable to set the sort order at this time.  Please try again in a few minutes.');
                    }
                });
            },
            helper: fixHelper
        }).disableSelection();
    ";

Yii::app()->clientScript->registerScript('sortable-project', $str_js);
?>
<?php
$this->breadcrumbs = array(
    'Immobili' => array('index'),
    'Lista',
);

$this->menu = array(
//array('label'=>'List Immobile','url'=>array('index')),
    array('label' => 'Nuovo Immobile', 'url' => array('create')),
);
?>

<h1>Lista Immobili</h1>

<?php
$this->widget('booster.widgets.TbGridView', array(
    'id' => 'immobile-grid',
    'dataProvider' => $model->search(),
    'rowCssClassExpression' => '"items[]_{$data->id}"',
    'filter' => $model,
    'columns' => array(
        //     'id',
        array('name' => 'id_localita', 'value' => '$data->getLocalita()', 'filter' => $model->getLocalitas()),
        'tipologia',
        'indirizzo',
        'prezzo',
        array('name' => 'pubblicato', 'value' => '$data->pubblicato == 1 ?"SI":"NO"', 'filter' => array(0 => 'NO', 1 => 'SI')),
        //   'informazioni',
        /*
          'metratura',
          'piano',
          'locali',
          'bagno',
          'esterno',
          'ascensore',
          'riscaldamento',
          'giardino',
          'box',
          'classe_energetica',
          'realizzazione',
          'latitudine',
          'longitudine',
          'pubblicato',
          'posizione',
          'eliminato',
         */
        array(
            'class' => 'booster.widgets.TbButtonColumn',
        ),
    ),
));
?>
