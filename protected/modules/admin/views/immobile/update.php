<?php
$this->breadcrumbs = array(
    'Immobili' => array('index'),
    $model->id => array('view', 'id' => $model->id),
    'Modifica',
);

$this->menu = array(
    array('label' => 'Lista Immobili', 'url' => array('index')),
    array('label' => 'Nuovo Immobile', 'url' => array('create')),
    array('label' => 'Vedi Immobile', 'url' => array('view', 'id' => $model->id)),
//	array('label'=>'Manage Immobile','url'=>array('admin')),
);
?>

<h1>Modifica immobile <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>