<?php
$this->breadcrumbs = array(
    'Immobili' => array('index'),
    'Nuovo',
);

$this->menu = array(
    array('label' => 'Lista Immobili', 'url' => array('index')),
//array('label'=>'Manage Immobile','url'=>array('admin')),
);
?>

<h1>Nuovo Immobile</h1>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>