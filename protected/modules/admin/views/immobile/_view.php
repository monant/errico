<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_localita')); ?>:</b>
	<?php echo CHtml::encode($data->id_localita); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipologia')); ?>:</b>
	<?php echo CHtml::encode($data->tipologia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('indirizzo')); ?>:</b>
	<?php echo CHtml::encode($data->indirizzo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prezzo')); ?>:</b>
	<?php echo CHtml::encode($data->prezzo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('informazioni')); ?>:</b>
	<?php echo CHtml::encode($data->informazioni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('metratura')); ?>:</b>
	<?php echo CHtml::encode($data->metratura); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('piano')); ?>:</b>
	<?php echo CHtml::encode($data->piano); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('locali')); ?>:</b>
	<?php echo CHtml::encode($data->locali); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bagno')); ?>:</b>
	<?php echo CHtml::encode($data->bagno); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('esterno')); ?>:</b>
	<?php echo CHtml::encode($data->esterno); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ascensore')); ?>:</b>
	<?php echo CHtml::encode($data->ascensore); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('riscaldamento')); ?>:</b>
	<?php echo CHtml::encode($data->riscaldamento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('giardino')); ?>:</b>
	<?php echo CHtml::encode($data->giardino); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('box')); ?>:</b>
	<?php echo CHtml::encode($data->box); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('classe_energetica')); ?>:</b>
	<?php echo CHtml::encode($data->classe_energetica); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('realizzazione')); ?>:</b>
	<?php echo CHtml::encode($data->realizzazione); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('latitudine')); ?>:</b>
	<?php echo CHtml::encode($data->latitudine); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('longitudine')); ?>:</b>
	<?php echo CHtml::encode($data->longitudine); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pubblicato')); ?>:</b>
	<?php echo CHtml::encode($data->pubblicato); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('posizione')); ?>:</b>
	<?php echo CHtml::encode($data->posizione); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('eliminato')); ?>:</b>
	<?php echo CHtml::encode($data->eliminato); ?>
	<br />

	*/ ?>

</div>