<?php
$this->breadcrumbs=array(
	'Località'=>array('index'),
	'Nuova',
);

$this->menu=array(
array('label'=>'Lista Località','url'=>array('index')),
//array('label'=>'Manage Localita','url'=>array('admin')),
);
?>

<h1>Nuova Località</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>