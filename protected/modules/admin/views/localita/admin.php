<?php
Yii::app()->clientScript->registerScriptFile('http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js');
$str_js = "
        var fixHelper = function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        };
 
        $('#localita-grid table.items tbody').sortable({
            forcePlaceholderSize: true,
            forceHelperSize: true,
            items: 'tr',
            update : function () {
                serial = $('#localita-grid table.items tbody').sortable('serialize', {key: 'items[]', attribute: 'class'});
                $.ajax({
                    'url': '" . $this->createUrl('/admin/localita/sort') . "',
                    'type': 'post',
                    'data': serial,
                    'success': function(data){
                    },
                    'error': function(request, status, error){
                        alert('We are unable to set the sort order at this time.  Please try again in a few minutes.');
                    }
                });
            },
            helper: fixHelper
        }).disableSelection();
    ";

Yii::app()->clientScript->registerScript('sortable-project', $str_js);
?>
<?php
$this->breadcrumbs = array(
    'Localita' => array('index'),
    'Manage',
);

$this->menu = array(
    //array('label' => 'List Localita', 'url' => array('index')),
    array('label' => 'Nuova Localita', 'url' => array('create')),
);
?>

<h1>Lista Località</h1>


<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->widget('booster.widgets.TbGridView', array(
    'id' => 'localita-grid',
    'dataProvider' => $model->search(),
    'rowCssClassExpression' => '"items[]_{$data->id}"',
    'filter' => $model,
    'columns' => array(
        //'id',
        'nome',
       // 'posizione',
        array(
            'class' => 'booster.widgets.TbButtonColumn',
        ),
    ),
));
?>
