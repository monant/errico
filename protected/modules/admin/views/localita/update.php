<?php
$this->breadcrumbs = array(
    'Località' => array('index'),
    $model->nome => array('view', 'id' => $model->id),
    'Modifica',
);

$this->menu = array(
    array('label' => 'Lista Località', 'url' => array('index')),
    array('label' => 'Nuova Località', 'url' => array('create')),
    array('label' => 'Vedi Località', 'url' => array('view', 'id' => $model->id)),
    //array('label' => 'Manage Localita', 'url' => array('admin')),
);
?>

<h1>Modifica <?php echo $model->nome; ?></h1>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>