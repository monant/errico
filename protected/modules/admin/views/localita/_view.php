<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nome')); ?>:</b>
	<?php echo CHtml::encode($data->nome); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('posizione')); ?>:</b>
	<?php echo CHtml::encode($data->posizione); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('eliminato')); ?>:</b>
	<?php echo CHtml::encode($data->eliminato); ?>
	<br />


</div>