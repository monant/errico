<?php
$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
    'id' => 'localita-form',
    'enableAjaxValidation' => false,
        ));
?>

<p class="help-block">I campi con <span class="required">*</span> sono obbligatori.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldGroup($model, 'nome', array('widgetOptions' => array('htmlOptions' => array('class' => 'span5', 'maxlength' => 256)))); ?>

<div class="form-actions">
    <?php
    $this->widget('booster.widgets.TbButton', array(
        'buttonType' => 'submit',
        'context' => 'primary',
        'label' => $model->isNewRecord ? 'Aggiungi' : 'Modifica',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
