<?php
$this->breadcrumbs = array(
    'Località' => array('index'),
    $model->nome,
);

$this->menu = array(
    array('label' => 'Lista Località', 'url' => array('index')),
    array('label' => 'Nuova Località', 'url' => array('create')),
    array('label' => 'Modifica Località', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Elimina Località', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Sei sicuro?')),
);
?>

<h1>Vedi <?php echo $model->nome; ?></h1>

<?php
$this->widget('booster.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'nome',
        'posizione',
      //  'eliminato',
    ),
));
?>
