<?php
$this->breadcrumbs=array(
	'Valori Defaults'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List ValoriDefault','url'=>array('index')),
array('label'=>'Create ValoriDefault','url'=>array('create')),
array('label'=>'Update ValoriDefault','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete ValoriDefault','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage ValoriDefault','url'=>array('admin')),
);
?>

<h1>View ValoriDefault #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'field1',
		'field2',
		'field3',
		'field4',
),
)); ?>
