<?php
$this->breadcrumbs=array(
	'Valori Defaults'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List ValoriDefault','url'=>array('index')),
array('label'=>'Manage ValoriDefault','url'=>array('admin')),
);
?>

<h1>Create ValoriDefault</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>