<?php
$this->breadcrumbs = array(
    'Valori' => array('index'),
    $model->id => array('view', 'id' => $model->id),
    'Modifica',
);

$this->menu = array(
//    array('label' => 'List ValoriDefault', 'url' => array('index')),
//    array('label' => 'Create ValoriDefault', 'url' => array('create')),
//    array('label' => 'View ValoriDefault', 'url' => array('view', 'id' => $model->id)),
//    array('label' => 'Manage ValoriDefault', 'url' => array('admin')),
);
?>

<h1>Modifica Valori </h1>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>