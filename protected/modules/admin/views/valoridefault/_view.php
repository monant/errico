<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('field1')); ?>:</b>
	<?php echo CHtml::encode($data->field1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('field2')); ?>:</b>
	<?php echo CHtml::encode($data->field2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('field3')); ?>:</b>
	<?php echo CHtml::encode($data->field3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('field4')); ?>:</b>
	<?php echo CHtml::encode($data->field4); ?>
	<br />


</div>