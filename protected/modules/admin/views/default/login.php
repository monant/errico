<div class="row" style="padding-top: 20px;">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-log-in"></span> <?php echo Yii::app()->name ?></h3>
            </div>
            <div class="panel-body">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'login-form',
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
                    'htmlOptions' => array(
                        'role' => 'form',
                    ),
                ));
                ?>
                <?php echo $form->errorSummary($model); ?>
                <fieldset>
                    <div class="form-group">
                        <?php echo $form->textField($model, 'username', array('class' => 'form-control', 'placeholder' => 'username')); ?>
                    </div>
                    <div class="form-group">
                        <?php echo $form->passwordField($model, 'password', array('class' => 'form-control', 'placeholder' => 'password')); ?>
                    </div>
                    <input class="btn btn-lg btn-primary btn-block" type="submit" value="Login">
                </fieldset>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    #mainmenu, #footer{
        display: none;
    }
    .col-sm-10{
        width: 100%;
    }
</style>