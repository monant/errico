<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main_admin.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print_admin.css" media="print"/>        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>

    <body>

        <div id="wrap">

            <div id="mainmenu">
                <?php
                $this->widget(
                        'booster.widgets.TbNavbar', array(
                    'brand' => 'Ing. Errico',
                    'brandUrl' => $this->createUrl('/admin'),
                    'fixed' => false,
                    'fluid' => true,
                    'items' => array(
                        array(
                            'class' => 'booster.widgets.TbMenu',
                            'type' => 'navbar',
                            'items' => array(
                                array('label' => 'Valori', 'url' => array('/admin/valoridefault/update/id/1')),
                                array('label' => 'Località', 'url' => array('/admin/localita/index')),
                                array('label' => 'Immobili', 'url' => array('/admin/immobile/index')),
                                array('label' => 'Logout (' . Yii::app()->user->name . ')', 'url' => array('/admin/default/logout'), 'visible' => !Yii::app()->user->isGuest)
                            )
                        )
                    )
                        )
                );
                ?>
            </div><!-- mainmenu -->
            <div class="container-fluid">
                <?php echo $content; ?>
            </div>
            <div class="clear"></div>
            <div id="push"></div>
        </div>
        <div id="footer">
            <div class="container-fluid">
                <p>Copyright &copy; <?php echo date('Y'); ?> by <a href="http://www.farkell.com" target="_blank">Farkell</a></p>
            </div>
        </div>



    </body>
</html>
