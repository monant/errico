<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/main'); ?>
<div class="row">

    <div class="col-sm-10 col-xs-12">
        <div id="content">
            <?php echo $content; ?>
        </div><!-- content -->
    </div>
    <div class="col-sm-2 col-xs-12 bs-docs-sidebar">
        <div id="sidebar">
            <?php
            $this->beginWidget('zii.widgets.CPortlet', array(
                'title' => 'Operazioni',
            ));
            $this->widget(
                    'booster.widgets.TbMenu', array(
                'type' => 'list',
                'items' => 
                    $this->menu                
                    )
            );
            $this->endWidget();
            ?>
        </div><!-- sidebar -->
    </div>
</div>
<?php $this->endContent(); ?>